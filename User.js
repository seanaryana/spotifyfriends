'use strict';

let cookie;
let name;
let pictureUrl;
let accessToken;
let refreshToken;

class User {

  constructor(cookie, name, pictureUrl, accessToken, refreshToken) {
    this.cookie = cookie;
    this.name = name;
    this.pictureUrl = pictureUrl;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
  }
}

module.exports = User;
