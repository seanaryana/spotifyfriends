'use strict';

// Files from me
const randomString = require('./randomString');
const User = require('./User.js');

// External Deps
const express = require('express');
const request = require('request');
const querystring = require('querystring');
const cookieParser = require('cookie-parser');

// Node deps
const fs = require('fs');

// Constants
const client_id = process.env.CLIENT_ID;
const client_secret = process.env.CLIENT_SECRET;
const redirect_uri = 'https://spotify-friends-sharing.herokuapp.com/callback';
const scope = 'user-read-private user-read-email user-top-read';
const COOKIE_STRING = 'spotify_auth_state';
const QUERY_TYPES = ['artists', 'tracks'];
const QUERY_TIME_RANGE = ['short_term', 'medium_term', 'long_term'];
const DATA_DIR = 'data/'
const SONGS_FILE = DATA_DIR + 'songs.json';

// Bad programming
// cookie -> User
const UserMap = {};
// userName -> entries
// entires -> {type, data}
// data -> {mappings: {name : position}, names: [names]}
const UserData = {};
// Mapping of song to artist
const Songs = fs.existsSync(SONGS_FILE) ? JSON.parse(fs.readFileSync(SONGS_FILE)) : {};

const app = express();

app.use(express.static(__dirname + '/public/'))
   .use(cookieParser())
   .use('/favicon.ico', express.static(__dirname + '/public/favicon.ico'));

if (fs.existsSync(DATA_DIR)) {
  const files = fs.readdirSync(DATA_DIR);
  console.log('Reading ' + files);
  for (let i = 0; i < files.length; i++) {
    const name = files[i];
    const full = name.split('_');
    const first = full[0];
    const last = full.length > 1 ? full[1].split('.')[0] : '';
    const file = DATA_DIR + name;
    console.log(file);
    const content = fs.readFileSync(file, 'utf8');

    if (content) {
      UserData[first + ' ' + last] = JSON.parse(content);
    }
  }
} else {
  fs.mkdirSync(DATA_DIR);
}

app.get('/', (req, res) => {
  if (req.cookies[COOKIE_STRING] && UserMap[req.cookies[COOKIE_STRING]]) {
    console.log('Request from ' + UserMap[req.cookies[COOKIE_STRING]].name);
    res.redirect('/data');
    return;
  }
  console.log('Authorizing User');
  const state = randomString.generateRandomString(16);
  res.cookie(COOKIE_STRING, state);

  // your application requests authorization
  res.redirect('https://accounts.spotify.com/authorize?' +
    querystring.stringify({
      response_type: 'code',
      client_id: client_id,
      scope: scope,
      redirect_uri: redirect_uri,
      state: state
    }));
});

app.get('/callback', function(req, res) {

  // your application requests refresh and access tokens
  // after checking the state parameter

  const code = req.query.code || null;
  const state = req.query.state || null;
  const storedState = req.cookies ? req.cookies[COOKIE_STRING] : null;
  if (state === null || state !== storedState) {
    console.log(state);
    console.log(storedState);
  } else {
    var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
        code: code,
        redirect_uri: redirect_uri,
        grant_type: 'authorization_code'
      },
      headers: {
        'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
      },
      json: true
    };

    request.post(authOptions, function(error, response, body) {
      if (!error && response.statusCode === 200) {
        var accessToken = body.access_token,
            refreshToken = body.refresh_token;

        var options = {
          url: 'https://api.spotify.com/v1/me',
          headers: { 'Authorization': 'Bearer ' + accessToken },
          json: true
        };

        // use the access token to access the Spotify Web API
        request.get(options, function(error, response, body) {
          // console.log(body);
          const imageUrl = body.images ? body.images[0] ? body.images[0].url : '' : '';
          let name = body.display_name;
          if (!body.display_name) {
            name = 'No Name';
          }
          const user = new User(storedState, name, imageUrl, accessToken, refreshToken);
          console.log(user);
          UserMap[storedState] = user;
          fetchAndStore(user);
          res.redirect('/data');
        });
      }
    });
  }
});

app.get('/data', function(req, res) {
  sendAllUserData(res);
});

function fetchAndStore(user) {
  for (let i = 0; i < QUERY_TYPES.length; i++) {
    const type = QUERY_TYPES[i];
    for (let j = 0; j < QUERY_TIME_RANGE.length; j++) {
      const range = QUERY_TIME_RANGE[j];
      const url = 'https://api.spotify.com/v1/me/top/' + type + '?' +
      querystring.stringify({
        limit: 20,
        time_range: range,
      });
      let options = {
        url: url,
        headers: { 'Authorization': 'Bearer ' + user.accessToken },
        json: true
      };
      console.log('fetching data for ' + user.name);
      request.get(options, (error, response, body) => {
        if (error) {
          res.send(error);
        }
        const allData = {
          mappings: {},
          names: [],
        };
        for (let k = 0; k < body.items.length; k++) {
          const item = body.items[k];
          allData.mappings[item.name] = k;
          allData.names.push(item.name);
          if (item.type === 'track') {
            Songs[item.name] = getArtists(item.artists);
          }
        }
        let userData = UserData[user.name];
        if (!userData) {
          UserData[user.name] = {
            entries : [],
          };
          userData = UserData[user.name];
        }
        console.log('got data for ' + user.name + type + '_' + range);
        userData.entries.push({
          type: type + '_' + range,
          data: allData,
        });
        if (userData && userData.entries.length == 6) {
          saveData(user.name);
        } else {
          console.log('Got data for ' + userData.entries.length + '/6');
        }
      });
    };
  };
}

function getArtists(artists) {
  const names = [];
  artists.forEach((artist) => {
    console.log(artist);
    names.push(artist.name);
  });
  return names.join(', ');
}

function saveData(name, userData) {
  fs.writeFile(DATA_DIR + name.replace(' ', '_') + '.json', JSON.stringify(userData), (err) => {
    if (err) {
      console.log(err);
    }
    console.log('saved data for ' + name);
  });
  fs.writeFile(SONGS_FILE, JSON.stringify(Songs), (err) => {
    if (err) console.log(err);
  });
}

function sendAllUserData(res) {
  let html = '<html>';
  html += '<head></head>';
  const users = Object.keys(UserData);
  const sharedData = {
    artists_short_term: {},
    artists_medium_term: {},
    artists_long_term: {},
    tracks_short_term: {},
    tracks_medium_term: {},
    tracks_long_term: {},
  };
  // type -> data
  // data -> {song : people}
  for (let i = 0; i < users.length; i++) {
    const userName = users[i];
    const user = UserData[userName];
    html += '<h1>' + userName + '</h1>';
    if (!user.entries) {
      continue;
    }
    for (let j = 0; j < user.entries.length; j++) {
      const userData = user.entries[j];
      const type = userData.type;
      const data = userData.data;
      const names = data.names;
      html += '<h2>' + type + '</h2>';
      html += '<ol>'
      const shared = sharedData[type];
      for (let k = 0; k < names.length; k++) {
        const item = names[k];
        const artists = Songs[item];
        const artistsString = artists ? ' - ' + artists : '';
        let sharedName = shared[item];
        if (!sharedName) {
          sharedName = [];
          shared[item] = sharedName;
        }
        sharedName.push({name: userName, rank: data.mappings[item]+1});
        html += '<li>' + item + artistsString + '</li>';
      }
      html += '</ol>';
    }
  }
  const sharedKeys = Object.keys(sharedData);
  html += '<h1>Shared Data</h1>';
  for (let i = 0; i < sharedKeys.length; i++) {
    const sharedKey = sharedKeys[i];
    html += '<h2> Shared For ' + sharedKey + '</h2>';
    const trackData = sharedData[sharedKey];
    html += '<ol>'
    const trackList = Object.keys(trackData);
    for (let j = 0; j < trackList.length; j++) {
      const track = trackList[j];
      const artists = Songs[track];
      const artistsString = artists ? ' - ' + artists : '';
      const people = trackData[track];
      if (people.length > 1) {
        html += '<li>' + track + artistsString + '</li>';
        html += '<ul>';
        for (let k = 0; k < people.length; k++) {
          html += '<li>' + people[k].name + ' rank ' + people[k].rank + '</li>';
        }
        html += '</ul>';
      }
    }
    html += '</ol>'
  }

  html += '</html>';
  res.send(html);
}

const port = process.env.PORT || 8000;
console.log('Listening on ' + port);
app.listen(port);
