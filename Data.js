'use strict';

let name;
let position;

class Data {
  constructor(name, position) {
    this.name = name;
    this.position = position;
  }
}

module.exports = Data;
